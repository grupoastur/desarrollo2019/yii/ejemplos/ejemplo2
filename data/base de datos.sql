﻿CREATE DATABASE IF NOT EXISTS ejemplo2yii2;
USE ejemplo2yii2;

CREATE OR REPLACE TABLE catalogo(
  id int AUTO_INCREMENT,
  nombre varchar(100),
  descripcion varchar(100),
  PRIMARY KEY(id)
  );

INSERT INTO catalogo (id, nombre, descripcion)
  VALUES (1, 'flor1', 'flor del campo'),
  (2, 'flor2', 'flor del monte');